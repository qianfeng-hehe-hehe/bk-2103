package com.qf.config;

import com.qf.pojo.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 配置类, 配置类在tomcat启动就会自动加载运行, 只运行一次
 * @author zhaojian
 */
//相当于spring核心配置文件中的<beans></beans>标签
@Configuration
public class UserConfig {

    //相当于spring核心配置文件中声明<bean id=user1 class="xxxxx"></bean>
    @Bean(name = "user1")
    public User getUser() {
        return new User(1, "张三");
    }
}
