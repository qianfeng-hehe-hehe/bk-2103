package com.qf.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zhaojian
 */
@ConfigurationProperties(prefix = "aliyun")
@Component
@Data
public class AliYunConfig {

    private String ip;
    private String username;
    private String password;
}
