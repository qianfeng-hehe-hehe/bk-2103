package com.qf;

import com.qf.pojo.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * 启动类, 启动这个项目, 启动类必须在所有业务包平级目录或者上级目录
 * 原理:
 *      运行启动类中的main方法
 *      main方法调用底层的tomcat插件, 运行tomcat
 * @author zhaojian
 */
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

//    @Bean(name = "user2")
//    public User getUser() {
//        return new User(1, "李四");
//    }
}
