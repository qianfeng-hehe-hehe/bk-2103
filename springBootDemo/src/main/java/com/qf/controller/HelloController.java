package com.qf.controller;

import com.qf.config.AliYunConfig;
import com.qf.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author zhaojian
 */
@RestController
@RequestMapping("/test")
public class HelloController {

    @Resource(name = "user1")
    private User user;

    @Autowired
    private AliYunConfig aliYunConfig;

    @Value("${aliyun.username}")
    private String xxx;

    @RequestMapping("/hello")
    public String hello() {
        return "欢迎学习springboot!" + user;
    }

    @RequestMapping("/aliyun")
    public String aliyun() {
        return aliYunConfig.getIp() + "=====" +aliYunConfig.getUsername()+"====="+ aliYunConfig.getPassword();
    }
}
