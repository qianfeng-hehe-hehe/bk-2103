package com.qf.hello;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zhaojian
 */
//指定监听的队列名称
@RabbitListener(queues = "hello")
@Component
public class HelloListener {

    //接收消息处理消息的方法
    @RabbitHandler
    public void messageHandler(String msg, Channel channel, Message message) throws Exception {

        try {
            System.out.println("=========" + msg);
            //int i = 1/0;

            /**
             * 消费者发起成功通知
             * 第一个参数: DeliveryTag,消息的唯一标识  channel+消息编号
             * 第二个参数：是否开启批量处理 false:不开启批量
             * 举个栗子： 假设我先发送三条消息deliveryTag分别是5、6、7，可它们都没有被确认，
             *          当我发第四条消息此时deliveryTag为8，multiple设置为 true，会将5、6、7、8的消息全部进行确认。
             */
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),false);
        } catch (Exception e) {
            e.printStackTrace();

            /**
             * 返回失败通知
             * 第一个参数: DeliveryTag,消息的唯一标识  channel+消息编号
             * 第二个boolean true所有消费者都会拒绝这个消息，false代表只有当前消费者拒绝
             * 第三个boolean true消息接收失败重新回到原有队列中
             */
            channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true);
        }

    }
}
