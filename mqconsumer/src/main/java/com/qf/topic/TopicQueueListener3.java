package com.qf.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 接收队列中的消息, 交换器和队列使用代码创建
 * @author zhaojian
 */
@Component
public class TopicQueueListener3 {

    //接收消息处理消息的方法
    @RabbitListener(queues = "boot-queue")
    public void messageHandler(String msg) {
        System.out.println("====boot-queue=====" + msg);
    }
}
