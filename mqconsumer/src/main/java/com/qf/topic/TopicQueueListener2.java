package com.qf.topic;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zhaojian
 */
@RabbitListener(queues = "topic_queue2")
@Component
public class TopicQueueListener2 {

    //接收消息处理消息的方法
    @RabbitHandler
    public void messageHandler(String msg) {
        System.out.println("====topic_queue2=====" + msg);
    }
}
