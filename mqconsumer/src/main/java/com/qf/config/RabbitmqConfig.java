package com.qf.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author zhaojian
 */
@Configuration
public class RabbitmqConfig {
    /**
     * 1. 创建exchange - topic
     * 第一个参数: 交换器名称
     * 第二个参数: 交换器是否持久化, 也就是服务器重启交换器是否自动删除
     * 第三个参数: 如果没有消费者, 交换器是否自动删除
     */
    @Bean
    public TopicExchange getTopicExchange(){
        return new TopicExchange("boot-topic-exchange",false,false);
    }

    /**
     * 2. 创建queue
     * 第一个参数: 队列名称
     * 第二个参数: 队列是否持久化, 也就是服务器重启队列是否自动删除
     * 第三个参数: 是否排外的，有两个作用，
     *          1.当连接关闭时该队列是否会自动删除；
     *          2.该队列是否是私有的private，如果不是排外的，
     *              可以使用两个消费者都访问同一个队列，没有任何问题，如果是排外的，
     *              会对当前队列加锁，其他通道channel是不能访问的
     * 第四个参数: 队列是否自动删除, 也就是当没有消费者时, 队列是否自动删除
     * 第五个参数: 队列参数, 比如是否设置为延时队列等参数.
     */
    @Bean
    public Queue getQueue(){
        return new Queue("boot-queue",true,false,false,null);
    }

    /**
     * 3. 队列和交换器绑定在一起
     */
    @Bean
    public Binding getBinding(TopicExchange topicExchange,Queue queue){
        return BindingBuilder.bind(queue).to(topicExchange).with("*.red.*");
    }
}
