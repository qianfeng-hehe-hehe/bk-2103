package com.qf.routing;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zhaojian
 */
@RabbitListener(queues = "direct_queue1")
@Component
public class RoutingQueueListener1 {

    //接收消息处理消息的方法
    @RabbitHandler
    public void messageHandler(String msg) {
        System.out.println("====direct_queue1=====" + msg);
    }
}
