package com.qf.recive;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 保证接收消息不重复消费
 * @author zhaojian
 */
@RabbitListener(queues = "queue3")
@Component
public class ConsumerListener {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @RabbitHandler
    public void messageHandler(String msg, Channel channel, Message message) throws Exception {

        //1. 获取消息唯一编号
        String msgId = (String)message.getMessageProperties().getHeaders().get(
                "spring_returned_message_correlation");

        //2. 使用消息唯一编号作为key, 到redis中获取这个数据的状态
        /**
         * java中的方法叫做setIfAbsent, redis中的命令叫做setnx
         * 作用:
         *      如果为空就set值，并返回1, true
         *      如果存在(不为空)不进行操作，并返回0, false
         */
        Boolean flag = redisTemplate.opsForValue().setIfAbsent(msgId, "0", 10, TimeUnit.SECONDS);

        //3. 判断获取的状态
        if (flag) {
            //如果redis中存在这个消息状态, 正常处理业务
            System.out.println("=========处理业务, 进行增删改查!===========");

            //业务处理完成, 更改redis中的消息的处理状态
            redisTemplate.opsForValue().set(msgId, "1", 10, TimeUnit.SECONDS);

            //手动ack告诉mq服务器, 接收到了这个消息, 让mq服务器从队列中将这个消息删除
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        } else {
            //判断消息状态是否为1, 为1说明这个消息已经处理过, 手动ack, 不需要再次处理业务, 防止重复消费
            if ("1".equals(redisTemplate.opsForValue().get(msgId))) {
                //手动ack告诉mq服务器, 接收到了这个消息, 让mq服务器从队列中将这个消息删除
                channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            }
        }
    }

}
