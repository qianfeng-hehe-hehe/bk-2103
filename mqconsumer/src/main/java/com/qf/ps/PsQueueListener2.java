package com.qf.ps;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zhaojian
 */
@RabbitListener(queues = "ps_queue2")
@Component
public class PsQueueListener2 {

    //接收消息处理消息的方法
    @RabbitHandler
    public void messageHandler(String msg) {
        System.out.println("====ps_queue2=====" + msg);
    }
}
