package com.qf.work;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @author zhaojian
 */
@RabbitListener(queues = "work_queue")
@Component
public class WorkQueueListener1 {

    //接收消息处理消息的方法
    @RabbitHandler
    public void messageHandler(String msg) {
        System.out.println("====WorkQueueListener1=====" + msg);
    }
}
