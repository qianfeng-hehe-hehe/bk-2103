package com.qf.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author zhaojian
 */
@Data
//创建无参构造
@NoArgsConstructor
//创建全参构造
@AllArgsConstructor
public class User implements Serializable {

    private String id;

    private String name;

    private Integer age;
}
