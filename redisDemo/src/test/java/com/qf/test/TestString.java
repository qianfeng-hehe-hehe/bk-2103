package com.qf.test;

import com.alibaba.fastjson.JSON;
import com.qf.pojo.User;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Test;
import org.springframework.util.SerializationUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Pipeline;

/**
 * @author zhaojian
 */
public class TestString {

    /**
     * 测试字符串类型
     */
    @Test
    public void testString() {
        Jedis jedis = new Jedis("192.168.200.129", 6379);
        jedis.set("key3", "1");

        String key3 = jedis.get("key3");
        System.out.println("=====" + key3);
    }

    @Test
    public void testSetUser() {
        Jedis jedis = new Jedis("192.168.200.129", 6379);

        User user = new User("1", "青龙", 2000);
        byte[] key = SerializationUtils.serialize("user1");
        byte[] value = SerializationUtils.serialize(user);
        jedis.set(key, value);
    }

    @Test
    public void testGetUser() {
        Jedis jedis = new Jedis("192.168.200.129", 6379);
        byte[] key = SerializationUtils.serialize("user1");
        byte[] value = jedis.get(key);
        User user = (User) SerializationUtils.deserialize(value);
        System.out.println("======" + user);
    }


    @Test
    public void testSetJson() {
        Jedis jedis = new Jedis("192.168.200.129", 6379);
        User user = new User("1", "饕鬄", 2000);
        String value = JSON.toJSONString(user);
        jedis.set("user5", value);
    }

    @Test
    public void testGetJson() {
        Jedis jedis = new Jedis("192.168.200.129", 6379);
        String jsonStr = jedis.get("user5");
        User user = JSON.parseObject(jsonStr, User.class);
        System.out.println(user);
    }

    @Test
    public void testPool() {
        //1. 创建连接池配置信息
        GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
        poolConfig.setMaxTotal(100);  // 连接池中最大的活跃数
        poolConfig.setMaxIdle(10);   // 最大空闲数
        poolConfig.setMinIdle(5);   // 最小空闲数
        poolConfig.setMaxWaitMillis(3000);  // 当连接池空了之后,多久没获取到Jedis对象,就超时

        //2. 创建连接池
        JedisPool pool = new JedisPool(poolConfig,"192.168.200.129",6379);

        //3. 通过连接池获取jedis对象
        Jedis jedis = pool.getResource();


        //4. 操作
        jedis.set("test1", "111");

        jedis.close();
    }


    //  Redis管道的操作
    @Test
    public void pipeline(){
        //1. 创建连接池
        JedisPool pool = new JedisPool("192.168.200.129",6379);
        long l = System.currentTimeMillis();

        //2. 获取一个连接对象
        Jedis jedis = pool.getResource();

        //3. 执行incr - 100000次
        for (int i = 0; i < 100000; i++) {
            jedis.incr("pp");
        }

        //4. 释放资源
        jedis.close();

        //================================
        //2. 获取一个连接对象
//        Jedis jedis = pool.getResource();
//        //3. 创建管道
//        Pipeline pipelined = jedis.pipelined();
//        //3. 执行incr - 100000次放到管道中
//        for (int i = 0; i < 100000; i++) {
//            pipelined.incr("qq");
//        }
//        //4. 执行命令
//        pipelined.syncAndReturnAll();
//        //5. 释放资源
//        jedis.close();

        System.out.println(System.currentTimeMillis() - l);
    }

}
