package com.qf.controller;

import com.github.pagehelper.PageInfo;
import com.qf.pojo.User;
import com.qf.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhaojian
 */
@RestController
@RequestMapping("/user")
public class UserController {


    @Autowired
    private UserService userService;

    @RequestMapping("/all")
    public List<User> findAll() {
        List<User> list = userService.findUserAll();
        return list;
    }

    @RequestMapping("/page")
    public List<User> findPage(Integer pageNum) {
        PageInfo<User> userPage = userService.findUserPage(pageNum);
        List<User> list = userPage.getList();
        return list;
    }
}
