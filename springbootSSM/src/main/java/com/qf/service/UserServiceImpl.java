package com.qf.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaojian
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> findUserAll() {
        List<User> userAll = userMapper.findUserAll();
        return userAll;
    }

    @Override
    public PageInfo<User> findUserPage(Integer pageNum) {
        //1. 设置分页插件
        PageHelper.startPage(pageNum, 5);

        //2. 查询, select * from 表名   limit 从第几条开始查询, 每页查询条数
        List<User> list = userMapper.findUserAll();

        //3. 返回
        PageInfo<User> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

}
