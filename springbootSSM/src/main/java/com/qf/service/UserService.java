package com.qf.service;

import com.github.pagehelper.PageInfo;
import com.qf.mapper.UserMapper;
import com.qf.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author zhaojian
 */
public interface UserService {

    public List<User> findUserAll();

    public PageInfo<User> findUserPage(Integer pageNum);
}
