package com.qf.mapper;

import com.qf.pojo.User;

import java.util.List;

/**
 * @author zhaojian
 */
public interface UserMapper {

    public List<User> findUserAll();
}
