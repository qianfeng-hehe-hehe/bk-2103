package com.qf.test.routing;

import com.qf.ProducerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 路由模式, 定向发送模式
 * @author zhaojian
 */
@SpringBootTest(classes = ProducerApplication.class)
@RunWith(SpringRunner.class)
public class TestRouting {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend() {
        //第一个参数: 交换器名称, 第二个参数: 队列名称或者是路由键, 第三个参数:消息内容
        rabbitTemplate.convertAndSend("direct_exchange", "info", "这是测试路由模式, info!");
        rabbitTemplate.convertAndSend("direct_exchange", "error", "这是测试路由模式, error!");
    }

}
