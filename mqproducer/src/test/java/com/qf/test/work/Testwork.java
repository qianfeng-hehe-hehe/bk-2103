package com.qf.test.work;

import com.qf.ProducerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zhaojian
 */
@SpringBootTest(classes = ProducerApplication.class)
@RunWith(SpringRunner.class)
public class Testwork {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend() {
        for (int i =1; i <= 100000; i++){
            //第一个参数: 队列名称, 第二个参数:消息内容
            rabbitTemplate.convertAndSend("work_queue", "第一次使用简单工作模式发送消息!" + i);
        }
    }

}
