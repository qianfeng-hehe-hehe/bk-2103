package com.qf.test.ps;

import com.qf.ProducerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 发布订阅模式, 也叫做广播模式
 * @author zhaojian
 */
@SpringBootTest(classes = ProducerApplication.class)
@RunWith(SpringRunner.class)
public class TestPs {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend() {
        //第一个参数: 交换器名称, 第二个参数: 队列名称或者是路由键, 第三个参数:消息内容
        rabbitTemplate.convertAndSend("psExchange", "", "这是测试广播模式, 也就是发布订阅模式!");
    }

}
