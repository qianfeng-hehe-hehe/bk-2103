package com.qf.test.hello;

import com.qf.ProducerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author zhaojian
 */
@SpringBootTest(classes = ProducerApplication.class)
@RunWith(SpringRunner.class)
public class TestHello {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void testSend() {
        //第一个参数: 队列名称, 第二个参数:消息内容
        rabbitTemplate.convertAndSend("hello", "第一次使用简单工作模式发送消息!");
    }

}
