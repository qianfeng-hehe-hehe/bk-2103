package com.qf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhaojian
 */
@SpringBootApplication
public class ProducerApplication {

    /**
     * args参数一定要加, 作用是在线上运行的时候, 通过命令运行 java -jar  xxx.jar Xmx:128M
     * jvm优化的参数, 设置的堆内存大小, 栈内存大小等参数是通过args传入底层tomcat的, 如果不加这个参数
     * 以后线上服务器运行项目, 无法做参数优化.
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(ProducerApplication.class, args);
    }
}
