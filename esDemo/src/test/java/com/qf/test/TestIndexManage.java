package com.qf.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.util.EsClient;
import org.elasticsearch.action.admin.indices.alias.get.GetAliasesRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.common.xcontent.json.JsonXContent;
import org.junit.Test;
import pojo.Person;

import java.io.IOException;
import java.security.PublicKey;
import java.util.Date;

/**
 * 测试对es中的数据进行增删改
 * @author zhaojian
 */
public class TestIndexManage {

    //做json转换使用
    private ObjectMapper objectMapper = new ObjectMapper();

    //获取和es服务器连接对象
    private RestHighLevelClient esClient = EsClient.getClient();

    //定义索引名称
    private final static String INDEX = "person";

    /**
     * 创建主分片, 备份分片, 文档结构和类型
     * 此只能运行一次, 如果es服务器的索引库中存在这个索引则报错.
     */
    @Test
    public void testCreateIndex() throws Exception {
        //1. 创建主分片和备份分片
        Settings.Builder settings = Settings.builder()
                .put("number_of_shards", 5)
                .put("number_of_replicas", 1);


        //2. 创建索引结构
        XContentBuilder mapping = JsonXContent.contentBuilder()
                .startObject()
                    .startObject("properties")
                        .startObject("name")
                            .field("type", "text")
                        .endObject()
                        .startObject("age")
                            .field("type", "integer")
                        .endObject()
                        .startObject("birthday")
                            .field("type", "date")
                            .field("format", "yyyy-MM-dd")
                        .endObject()
                    .endObject()
                .endObject();

        //3. 将创建的索引和mapping映射结构关系组成请求对象
        CreateIndexRequest request = new CreateIndexRequest(TestIndexManage.INDEX)
                .settings(settings)
                .mapping(mapping);

        //4. 使用连接对象执行
        CreateIndexResponse response = esClient.indices().create(request, RequestOptions.DEFAULT);

        System.out.println("==========" + response);

    }


    /**
     * 测试检查索引是否存在
     */
    @Test
    public void testIndexExits() throws Exception {
        //1. 创建get请求对象
        GetAliasesRequest getRequest = new GetAliasesRequest();

        //2. 将索引的名字加入请求对象中
        getRequest.indices(TestIndexManage.INDEX);

        //3. 使用esClient对象去执行这个请求并返回响应
        boolean flag = esClient.indices().existsAlias(getRequest, RequestOptions.DEFAULT);

        //4. 打印响应内容
        System.out.println("==========" + flag);
    }

    /**
     * 删除整个索引结构, 包括删除主备分片以及内部的域等文档结构
     */
    @Test
    public void testDeleteIndex() throws Exception{
        //1. 创建删除索引的请求对象
        DeleteIndexRequest request = new DeleteIndexRequest();
        //2. 指定删除的索引名字
        request.indices(TestIndexManage.INDEX);

        //3. 通过esClient对象执行请求并返回响应
        AcknowledgedResponse response = esClient.indices().delete(request, RequestOptions.DEFAULT);

        //4. 打印响应结果
        System.out.println("========" + response.isAcknowledged());
    }

    /**
     * 添加和修改数据
     */
    @Test
    public void testCreateAndUpdateData() throws Exception {
        //1. 创建person数据对象
        Person person = new Person("1", "春梅", 23, new Date());

        //2. 将person对象转换成json格式
        String jsonStr = objectMapper.writeValueAsString(person);

        //3. 创建一个request请求对象, 指定索引的名字
        IndexRequest request = new IndexRequest(TestIndexManage.INDEX);

        //4. 手动指定id的值, 将json数据存入请求对象
        request.id(person.getId()).source(jsonStr, XContentType.JSON);

        //5. 通过esClient连接对象执行发送请求到服务器
        IndexResponse response = esClient.index(request, RequestOptions.DEFAULT);

        //6. 返回并打印结果
        System.out.println("==========" + response);
    }

    /**
     * 修改文档数据
     */
    @Test
    public void testUpdateDoc() throws Exception {
        //1. 准备一个person数据
        Person person = new Person("1", "青龙", 23, new Date());

        //2. 将person实体转换成json格式
        String json = objectMapper.writeValueAsString(person);

        //2. 创建修改请求对象, 第一个参数: 索引名字, 第二个参数:修改的文档id
        UpdateRequest request = new UpdateRequest(TestIndexManage.INDEX, person.getId());
        //设置修改后的内容, 第二个参数: 文档数据类型
        request.doc(json, XContentType.JSON);

        //3. 通过client对象执行
        UpdateResponse update = esClient.update(request, RequestOptions.DEFAULT);

        //4. 输出返回结果
        System.out.println(update.getResult().toString());

    }


    /**
     * 删除数据
     */
    @Test
    public void testDeleteDoc() throws Exception {
        //1. 创建删除去请求对象, 并指定需要删除的文档的索引名字和文档id
        DeleteRequest request = new DeleteRequest(TestIndexManage.INDEX, "1");

        //2. 执行请求并返回响应
        DeleteResponse response = esClient.delete(request, RequestOptions.DEFAULT);

        //3. 打印响应内容
        System.out.println("=========" + response);
    }


    /**
     * 批量添加文档数据
     */
    @Test
    public void testBulkCreateDoc() throws Exception {
        //1. 准备多个person对象数据
        Person person1 = new Person("1", "春梅", 23, new Date());
        Person person2 = new Person("2", "李瓶", 22, new Date());
        Person person3 = new Person("3", "金莲", 21, new Date());

        //2. 将多个person对象数据转换成json格式字符串
        String jsonStr1 = objectMapper.writeValueAsString(person1);
        String jsonStr2 = objectMapper.writeValueAsString(person2);
        String jsonStr3 = objectMapper.writeValueAsString(person3);

        //3. 创建批量执行请求对象
        BulkRequest bulkRequest = new BulkRequest();

        //4. 将所有的json格式数据放入请求对象中
        bulkRequest.add(new IndexRequest(TestIndexManage.INDEX).id(person1.getId()).source(jsonStr1, XContentType.JSON));
        bulkRequest.add(new IndexRequest(TestIndexManage.INDEX).id(person2.getId()).source(jsonStr2, XContentType.JSON));
        bulkRequest.add(new IndexRequest(TestIndexManage.INDEX).id(person3.getId()).source(jsonStr3, XContentType.JSON));

        //5. 使用esClient对象执行请求并返回结果
        BulkResponse response = esClient.bulk(bulkRequest, RequestOptions.DEFAULT);

        //6. 打印响应结果
        System.out.println("=========" + response);
    }


    /**
     * 批量删除文档数据
     * @throws IOException
     */
    @Test
    public void bulkDeleteDoc() throws IOException {
        //1. 封装Request对象
        BulkRequest request = new BulkRequest();
        request.add(new DeleteRequest(TestIndexManage.INDEX,"1"));
        request.add(new DeleteRequest(TestIndexManage.INDEX,"2"));
        request.add(new DeleteRequest(TestIndexManage.INDEX,"3"));

        //2. client执行
        BulkResponse resp = esClient.bulk(request, RequestOptions.DEFAULT);

        //3. 输出
        System.out.println(resp);
    }


}
