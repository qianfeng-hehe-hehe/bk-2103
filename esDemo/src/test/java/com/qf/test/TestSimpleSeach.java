package com.qf.test;

import com.qf.util.EsClient;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;

import java.util.Map;

/**
 * 简单查询
 * @author zhaojian
 */
public class TestSimpleSeach {

    //获取和es服务器连接对象
    private RestHighLevelClient esClient = EsClient.getClient();

    //定义索引名称
    private final static String INDEX = "book";

    /**
     * 根据词元查询, 也就是根据某个域指定一个单词进行查询
     * @throws Exception
     */
    @Test
    public void testTermQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestSimpleSeach.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(10);

        //5. 设置查询条件
        sourceBuilder.query(QueryBuilders.termsQuery("name", "鬼吹灯"));

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }


    /**
     * 查询所有数据
     * @throws Exception
     */
    @Test
    public void testMathAllQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestSimpleSeach.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(50);

        //5. =============== 设置查询条件 ======================
        sourceBuilder.query(QueryBuilders.matchAllQuery());

        //=====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }

    /**
     * 根据关键字先进行切分词, 将切分出来的词跟索引中的词进行匹配查询
     * @throws Exception
     */
    @Test
    public void testMathQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestSimpleSeach.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(50);

        //5. =============== 设置查询条件 ======================
        sourceBuilder.query(QueryBuilders.matchQuery("name", "鬼吹灯"));

        //=====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }

    /**
     * 根据关键词搜索, 如果关键词是多个, 那么多个关键词之间的搜索关系是and或者是or
     * @throws Exception
     */
    @Test
    public void testBooleanMathQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestSimpleSeach.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(50);

        //5. =============== 设置查询条件 ======================
        sourceBuilder.query(QueryBuilders.matchQuery("desc", "java 李瓶").operator(Operator.AND));

        //=====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }

    /**
     * 从多个域中进行查询
     * @throws Exception
     */
    @Test
    public void testMultiMathQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestSimpleSeach.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(50);

        //5. =============== 设置查询条件 ======================
        sourceBuilder.query(QueryBuilders.multiMatchQuery("鬼吹灯", "name", "desc"));

        //=====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }

    /**
     * 根据文档id查询指定的文档数据
     * @throws Exception
     */
    @Test
    public void testIdQuery() throws Exception {
        //1. 创建request查询对象
        GetRequest getRequest = new GetRequest(TestSimpleSeach.INDEX, "1");
        //2. 执行查询
        GetResponse response = esClient.get(getRequest, RequestOptions.DEFAULT);
        //3. 返回结果并打印
        System.out.println(response.getSourceAsMap());
    }

    /**
     * 根据多个id进行查询
     * @throws Exception
     */
    @Test
    public void testIdsQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestSimpleSeach.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(50);

        //5. =============== 设置查询条件 ======================
        sourceBuilder.query(QueryBuilders.idsQuery().addIds("1", "5", "7"));

        //=====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }

    /**
     * 根据数值范围进行查询
     * @throws Exception
     */
    @Test
    public void testRangeQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestSimpleSeach.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(50);

        //5. =============== 设置查询条件 ======================
        sourceBuilder.query(QueryBuilders.rangeQuery("count").gte("200000").lte("500000"));

        //=====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }

}
