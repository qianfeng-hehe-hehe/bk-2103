package com.qf.test;

import com.qf.util.EsClient;
import org.apache.lucene.search.BooleanQuery;
import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.metrics.Cardinality;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.junit.Test;

import java.util.Map;

/**
 * @author zhaojian
 */
public class TestHardSearch {

    //获取和es服务器连接对象
    private RestHighLevelClient esClient = EsClient.getClient();

    //定义索引名称
    private final static String INDEX = "book";


    /**
     * 组合条件查询, 布尔查询, 将多个查询条件查询出来的结果进行组合
     * must并且, should或者, must_not非
     * @throws Exception
     */
    @Test
    public void testBooleanQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestHardSearch.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(10);

        //5. ===================设置查询条件===================
        //创建组合查询对象
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        //创建词元查询
        boolQueryBuilder.must().add(QueryBuilders.termsQuery("name", "鬼吹灯"));
        //创建数值范围查询
        boolQueryBuilder.must().add(QueryBuilders.rangeQuery("count").gte(200000).lte(500000));

        //将组合查询对象放入总查询对象中
        sourceBuilder.query(boolQueryBuilder);
        //====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }

    /**
     * 过滤查询
     * 首先根据关键词查询出来结果集
     * 再加入其他过滤条件, 每次加入条件都从上一次结果集的数据中查询,
     * 查询结果越来越少, 越来越精确
     * @throws Exception
     */
    @Test
    public void testFilterQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestHardSearch.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(10);

        //5. ===================设置查询条件===================
        //创建组合查询对象
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        //创建词元查询
        boolQueryBuilder.must().add(QueryBuilders.termsQuery("name", "金瓶梅"));

        //加入过滤条件
        boolQueryBuilder.filter(QueryBuilders.rangeQuery("count").gte(1).lte(2000000));

        //将组合查询对象放入总查询对象中
        sourceBuilder.query(boolQueryBuilder);
        //====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }


    /**
     * 高亮查询
     * 查询结果中, 名字涉及到查询的关键字的需要变色显示
     * @throws Exception
     */
    @Test
    public void testHighLightQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestHardSearch.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(10);

        //5. ===================设置查询条件===================
        sourceBuilder.query(QueryBuilders.termsQuery("name", "鬼吹灯"));

        //创建高亮查询对象
        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //设置高亮显示的域名
        highlightBuilder.field("name");

        //设置高亮前缀
        highlightBuilder.preTags("<span style='color:red'>");
        //设置高亮后缀
        highlightBuilder.postTags("</span>");

        //将高亮查询对象放入查询条件中
        sourceBuilder.highlighter(highlightBuilder);

        //====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);

        //8. 从响应中提取查询结果遍历并展示
        if (response != null) {
            //查询到的总记录数
            TotalHits totalHits = response.getHits().getTotalHits();
            System.out.println("===查询到的总记录数====" + totalHits);

            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                Map<String, Object> map = hit.getSourceAsMap();
                HighlightField highlightField = hit.getHighlightFields().get("name");

                System.out.println(highlightField);
                String hightLightName = highlightField.fragments()[0].string();
                System.out.println("===高亮名字==" + hightLightName);
                System.out.println("===============================================");
                System.out.println("====" + map);
            }
        }
    }


    /**
     * 去重统计聚合
     * 对数据自动进行去重处理, 对去重后的结果集进行统计, 统计出一共有多少条不重复的数据
     * @throws Exception
     */
    @Test
    public void testCardinalityQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestHardSearch.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(10);

        //5. ===================设置聚合查询条件===================
        //cardAgg是给本次聚合起的名字, 作用是后面取聚合结果集需要根据这个名字获取结果
        sourceBuilder.aggregation(AggregationBuilders.cardinality("cardAgg").field("count"));
        //====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);


        if (response != null) {
            //获取聚合结果, 这里是统计不重复的数据一共多少条
            Cardinality agg = response.getAggregations().get("cardAgg");
            long count = agg.getValue();
            System.out.println("======" + count);
        }
    }

    /**
     * 根据时间范围统计
     * @throws Exception
     */
    @Test
    public void testTimeRangeQuery() throws Exception {
        //1. 创建request查询对象
        SearchRequest request = new SearchRequest(TestHardSearch.INDEX);

        //2. 创建查询条件对象
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        //3. 指定从第几条数据开始查询
        sourceBuilder.from(0);
        //4. 指定每页查询多少条数据
        sourceBuilder.size(10);

        //5. ===================设置聚合查询条件===================
        //agg是给本次聚合起的名字, 作用是后面取聚合结果集需要根据这个名字获取结果
        sourceBuilder.aggregation(AggregationBuilders.range("agg").field("count")
                                .addUnboundedTo(100000)   //统计0-100000的有多少条数据
                                .addRange(100000, 2000000)  //统计100000-2000000的有多少条数据
                                .addUnboundedFrom(2000000)); //统计2000000以上的有多少条数据
        //====================================================

        //6. 将查询条件对象放入请求对象中
        request.source(sourceBuilder);

        //7. 执行查询并返回响应
        SearchResponse response = esClient.search(request, RequestOptions.DEFAULT);


        if (response != null) {
            //获取聚合结果, 这里是统计不重复的数据一共多少条
            Range range = response.getAggregations().get("agg");
            for (Range.Bucket bucket : range.getBuckets()) {
                //统计条件显示
                String keyAsString = bucket.getKeyAsString();
                //统计结构
                long docCount = bucket.getDocCount();
                System.out.println("===keyAsString====" + keyAsString);
                System.out.println("===docCount====" + docCount);
                System.out.println("==========================================");
            }
        }
    }


    /**
     * 根据查询条件进行删除
     * @throws Exception
     */
    @Test
    public void testDeleteByQuery() throws Exception {
        //1. 创建删除请求对象
        DeleteByQueryRequest request = new DeleteByQueryRequest(TestHardSearch.INDEX);

        //2. 指定查询条件
        request.setQuery(QueryBuilders.termsQuery("name", "鬼吹灯"));

        //3. 执行删除
        BulkByScrollResponse response = esClient.deleteByQuery(request, RequestOptions.DEFAULT);

        //4. 打印返回的响应结果
        System.out.println(response.toString());
    }
}
